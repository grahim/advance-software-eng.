﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApplication1
{
		class Program
		{

			static void Main(string[] args)
			{



				double weight_in_kg; // initializing 
				double B_m_i;   // initializing 

				double height_in_meter;  // initializing 

				do
				{

					Console.WriteLine("Enter your weight(in kg):");
					weight_in_kg = Convert.ToInt32(Console.ReadLine());
					Console.Write("Weight in kg : {0}", weight_in_kg);


					Console.WriteLine("\n\nEnter your height(in meter):");
					height_in_meter = Convert.ToInt32(Console.ReadLine());
					Console.Write("Height in meter : {0}", height_in_meter);

					B_m_i = weight_in_kg / (height_in_meter * 2);
					Console.Write("\n\nYour BMI : {0}", B_m_i);

					//for gender....................
					//input gender and check for "Male", "Female" or "Unspecied gender"
					string gender = "";
					Console.Write("\nEnter gender(F for female and M  for male): ");
					gender = Console.ReadLine();

					if (gender.ToUpper() == "M" || gender.ToLower() == "M")
						Console.WriteLine(" male.");
					else if (gender.ToUpper() == "F" || gender.ToLower() == "F")
						Console.WriteLine("female.");
					else
						Console.WriteLine("Unspecified gender.");

					if (B_m_i < 17.5)
					{
						Console.WriteLine("anorexia");

					}
					if (B_m_i < 19.1 && gender == "f" || (B_m_i < 20.7 && gender == "m"))
					{
						Console.WriteLine("Underweight");

					}
					if (B_m_i >= 19.1 && B_m_i <= 25.8 && gender == "f" || (B_m_i >= 20.7 && B_m_i <= 26.4 && gender == "m"))
					{
						Console.WriteLine("In normal range.");

					}

					if (B_m_i >= 25.8 && B_m_i <= 27.3 && gender == "f" || (B_m_i >= 26.4 && B_m_i <= 27.8 && gender == "m"))
					{
						Console.WriteLine("marginally overweight.");

					}

					if (B_m_i >= 27.3 && B_m_i <= 32.3 && gender == "f" || (B_m_i >= 27.8 && B_m_i <= 31.1 && gender == "m"))
					{
						Console.WriteLine("overweight.");

					}

					if (B_m_i > 32.3 && gender == "f" || (B_m_i > 32.3 && gender == "m"))
					{
						Console.WriteLine("very overweight or obese.");

					}

					if (B_m_i >= 35 && B_m_i <= 40 && gender == "f" || (B_m_i >= 35 && B_m_i <= 40 && gender == "m"))
					{
						Console.WriteLine("severely obese.");

					}


					if (B_m_i >= 40 && B_m_i <= 50 && gender == "f" || (B_m_i >= 40 && B_m_i <= 50 && gender == "m"))
					{
						Console.WriteLine(" morbidly obese.");

					}

					if (B_m_i >= 50 && B_m_i <= 60 && gender == "f" || (B_m_i >= 50 && B_m_i <= 60 && gender == "m"))
					{
						Console.WriteLine("super obese.");

					}

					Console.WriteLine("Do you want to try again? Write Y for yes and N for no.");


				}
				while (Console.ReadLine() == "Y");



			}
		}
	}

