﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI_
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen mypen = new Pen(Color.Black, 2);
            g.DrawLine(mypen, 0, 0, 100, 100);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Point pt1 = new Point(0, 0);
            Point pt2 = new Point(200, 200);
            Rectangle rect1 = new Rectangle(50, 80, 100, 130);
            Graphics g = e.Graphics;
            Pen myPen = new Pen(Color.Gold, 5);

            g.DrawLine(myPen, pt1, pt2);
            g.DrawLine(myPen, 0, 50, 200, 50);

            g.DrawEllipse(myPen, 0, 50, 200, 100);

            g.DrawRectangle(myPen, rect1);
            myPen.Dispose();
        }
    }
}
