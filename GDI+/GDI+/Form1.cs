﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI_
{
    public partial class Form1 : Form
    {
        Bitmap btmap;
        Graphics g;
        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            btmap = new Bitmap(640, 480);
            Graphics WindowG = e.Graphics;
            Graphics g = Graphics.FromImage(btmap);

            Pen p = new Pen(Color.Red, 2);
            g.DrawLine(p, 0, 0, 640, 480);

            Graphics windowG = e.Graphics;
            WindowG.DrawImageUnscaled(btmap, 0, 0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btmap.Save("d:\\image1.jpg",ImageFormat.Jpeg);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = new Bitmap("d:\\image1.jpg");
        }
    }
}
